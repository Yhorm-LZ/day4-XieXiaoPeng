package com.afs.tdd;

import java.util.List;

public class MarsRover {
    private Location location;

    public MarsRover(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }

    public void executeCommandList(List<Command> commands){
        commands.forEach(this::executeOneCommand);
    }
    public void executeOneCommand(Command command){
        if(Command.MOVE.equals(command)) {
            executeCommandMove();
        } else if (Command.TURN_LEFT.equals(command)) {
            executeTurnLeft();
        }
        else{
            executeTurnRight();
        }
    }

    private void executeCommandMove(){
        if (Direction.NORTH.equals(this.location.getDirection())) {
            this.location.setCoordinateY(this.location.getCoordinateY() + 1);
        }
        else if(Direction.SOUTH.equals(this.location.getDirection())) {
            this.location.setCoordinateY(this.location.getCoordinateY() - 1);
        }
        else if(Direction.EAST.equals(this.location.getDirection())) {
            this.location.setCoordinateX(this.location.getCoordinateX() + 1);
        }
        else{
            this.location.setCoordinateX(this.location.getCoordinateX() - 1);
        }
    }
    private void executeTurnLeft(){
        if(Direction.NORTH.equals(this.location.getDirection())){
            this.location.setDirection(Direction.WEST);
        }
        else if(Direction.EAST.equals(this.location.getDirection())){
            this.location.setDirection(Direction.NORTH);
        }
        else if(Direction.SOUTH.equals(this.location.getDirection())){
            this.location.setDirection(Direction.EAST);
        }
        else{
            this.location.setDirection(Direction.SOUTH);
        }
    }
    private void executeTurnRight() {
        if(Direction.EAST.equals(this.location.getDirection())){
            this.location.setDirection(Direction.SOUTH);
        }
        else if(Direction.SOUTH.equals(this.location.getDirection())){
            this.location.setDirection(Direction.WEST);
        }
        else if(Direction.WEST.equals(this.location.getDirection())){
            this.location.setDirection(Direction.NORTH);
        }
        else{
            this.location.setDirection(Direction.EAST);
        }
    }
}
