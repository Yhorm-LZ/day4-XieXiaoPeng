package com.afs.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class MarsRoverTest {
    @Test
    void should_change_0_1_North_when_executeCommand_given_location_0_0_North_and_command_move(){
        Location marsRoverOriginalLocation=new Location(0,0,Direction.NORTH);
        MarsRover marsRover=new MarsRover(marsRoverOriginalLocation);
        marsRover.executeOneCommand(Command.MOVE);

        Location expected=new Location(0,1,Direction.NORTH);

        Assertions.assertEquals(expected,marsRover.getLocation());
    }
    @Test
    void should_change_1_0_East_when_executeCommand_given_location_0_0_East_and_command_move(){
        Location marsRoverOriginalLocation=new Location(0,0,Direction.EAST);
        MarsRover marsRover=new MarsRover(marsRoverOriginalLocation);
        marsRover.executeOneCommand(Command.MOVE);

        Location expected=new Location(1,0,Direction.EAST);

        Assertions.assertEquals(expected,marsRover.getLocation());
    }
    @Test
    void should_change_0_negative_1_South_when_executeCommand_given_location_0_0_South_and_command_move(){
        Location marsRoverOriginalLocation=new Location(0,0,Direction.SOUTH);
        MarsRover marsRover=new MarsRover(marsRoverOriginalLocation);
        marsRover.executeOneCommand(Command.MOVE);

        Location expected=new Location(0,-1,Direction.SOUTH);

        Assertions.assertEquals(expected,marsRover.getLocation());
    }
    @Test
    void should_change_negative_1_0_West_when_executeCommand_given_location_0_0_West_and_command_move(){
        Location marsRoverOriginalLocation=new Location(0,0,Direction.WEST);
        MarsRover marsRover=new MarsRover(marsRoverOriginalLocation);
        marsRover.executeOneCommand(Command.MOVE);

        Location expected=new Location(-1,0,Direction.WEST);

        Assertions.assertEquals(expected,marsRover.getLocation());
    }
    @Test
    void should_change_0_0_West_when_executeCommand_given_location_0_0_North_and_command_turn_left(){
        Location marsRoverOriginalLocation=new Location(0,0,Direction.NORTH);
        MarsRover marsRover=new MarsRover(marsRoverOriginalLocation);
        marsRover.executeOneCommand(Command.TURN_LEFT);

        Location expected=new Location(0,0,Direction.WEST);

        Assertions.assertEquals(expected,marsRover.getLocation());
    }
    @Test
    void should_change_0_0_North_when_executeCommand_given_location_0_0_East_and_command_turn_left(){
        Location marsRoverOriginalLocation=new Location(0,0,Direction.EAST);
        MarsRover marsRover=new MarsRover(marsRoverOriginalLocation);
        marsRover.executeOneCommand(Command.TURN_LEFT);

        Location expected=new Location(0,0,Direction.NORTH);

        Assertions.assertEquals(expected,marsRover.getLocation());
    }
    @Test
    void should_change_0_0_East_when_executeCommand_given_location_0_0_South_and_command_turn_left(){
        Location marsRoverOriginalLocation=new Location(0,0,Direction.SOUTH);
        MarsRover marsRover=new MarsRover(marsRoverOriginalLocation);
        marsRover.executeOneCommand(Command.TURN_LEFT);

        Location expected=new Location(0,0,Direction.EAST);

        Assertions.assertEquals(expected,marsRover.getLocation());
    }
    @Test
    void should_change_0_0_South_when_executeCommand_given_location_0_0_West_and_command_turn_left(){
        Location marsRoverOriginalLocation=new Location(0,0,Direction.WEST);
        MarsRover marsRover=new MarsRover(marsRoverOriginalLocation);
        marsRover.executeOneCommand(Command.TURN_LEFT);

        Location expected=new Location(0,0,Direction.SOUTH);

        Assertions.assertEquals(expected,marsRover.getLocation());
    }
    @Test
    void should_change_0_0_South_when_executeCommand_given_location_0_0_East_and_command_turn_right(){
        Location marsRoverOriginalLocation=new Location(0,0,Direction.EAST);
        MarsRover marsRover=new MarsRover(marsRoverOriginalLocation);
        marsRover.executeOneCommand(Command.TURN_RIGHT);

        Location expected=new Location(0,0,Direction.SOUTH);

        Assertions.assertEquals(expected,marsRover.getLocation());
    }
    @Test
    void should_change_0_0_West_when_executeCommand_given_location_0_0_South_and_command_turn_right(){
        Location marsRoverOriginalLocation=new Location(0,0,Direction.SOUTH);
        MarsRover marsRover=new MarsRover(marsRoverOriginalLocation);
        marsRover.executeOneCommand(Command.TURN_RIGHT);

        Location expected=new Location(0,0,Direction.WEST);

        Assertions.assertEquals(expected,marsRover.getLocation());
    }
    @Test
    void should_change_0_0_North_when_executeCommand_given_location_0_0_West_and_command_turn_right(){
        Location marsRoverOriginalLocation=new Location(0,0,Direction.WEST);
        MarsRover marsRover=new MarsRover(marsRoverOriginalLocation);
        marsRover.executeOneCommand(Command.TURN_RIGHT);

        Location expected=new Location(0,0,Direction.NORTH);

        Assertions.assertEquals(expected,marsRover.getLocation());
    }
    @Test
    void should_change_0_0_East_when_executeCommand_given_location_0_0_North_and_command_turn_right(){
        Location marsRoverOriginalLocation=new Location(0,0,Direction.NORTH);
        MarsRover marsRover=new MarsRover(marsRoverOriginalLocation);
        marsRover.executeOneCommand(Command.TURN_RIGHT);

        Location expected=new Location(0,0,Direction.EAST);

        Assertions.assertEquals(expected,marsRover.getLocation());
    }
    @Test
    void should_change_negative_1_1_North_when_executeCommand_given_location_0_0_North_and_commands_move_turnLeft_move_turnRight(){
        List<Command> commands = new ArrayList<>();
        commands.add(Command.MOVE);
        commands.add(Command.TURN_LEFT);
        commands.add(Command.MOVE);
        commands.add(Command.TURN_RIGHT);
        Location marsRoverOriginalLocation=new Location(0,0,Direction.NORTH);
        MarsRover marsRover=new MarsRover(marsRoverOriginalLocation);

        marsRover.executeCommandList(commands);

        Location expected=new Location(-1,1,Direction.NORTH);
        Assertions.assertEquals(expected,marsRover.getLocation());
    }
}
